//
//  PokedexInterface.swift
//  Pokedex
//
//  Created by Rodrigo Miranda Castillo on 31/08/23.
//

import Foundation

protocol PokedexViewModel: AnyObject{
    func updateUI()
}
