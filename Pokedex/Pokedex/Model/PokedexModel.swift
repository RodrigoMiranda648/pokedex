//
//  PokedexModel.swift
//  Pokedex
//
//  Created by Rodrigo Miranda Castillo on 31/08/23.
//

import Foundation

struct PokedexModel: Decodable{
    var name: String
    var height: Int
    var sprites: Sprites
    var types: [Types]
    var weight: Int
}

struct Sprites: Decodable{
    var other: Other
}
struct Types: Decodable{
    var type: PokeType
}
struct PokeType: Decodable{
    var name: String
}

struct Other: Decodable{
    var officialArtwork: OfficialArtwork
        
    enum CodingKeys: String, CodingKey {
        case officialArtwork = "official-artwork"
    }
}

struct OfficialArtwork: Decodable{
    var frontDefault: String
        
    enum CodingKeys: String, CodingKey {
        case frontDefault = "front_default"
    }
}
