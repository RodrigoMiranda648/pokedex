//
//  PokedexViewModel.swift
//  Pokedex
//
//  Created by Rodrigo Miranda Castillo on 31/08/23.
//

import Foundation
import UIKit

class PokedexViewModelImp{
    var delegate: PokedexViewModel?
    var pokemonName: String?
    var pokemonImage: UIImage?
    var pokemonType: String?
    var pokemonTypeTranslated: String?
    var pokemonHeight: String?
    var pokemonWeight: String?
    var timerFunction: DispatchWorkItem?
    let translateOptions = [
        "bug"       : "bicho",
        "dark"      : "oscuro",
        "dragon"    : "dragón",
        "electric"  : "eléctrico",
        "fairy"     : "hada",
        "fighting"  : "pelea",
        "fire"      : "fuego",
        "flying"    : "vuelo",
        "ghost"     : "fantasma",
        "grass"     : "hierba",
        "ground"    : "tierra",
        "ice"       : "hielo",
        "poison"    : "veneno",
        "psychic"   : "psíquico",
        "rock"      : "roca",
        "steel"     : "acero",
        "water"     : "agua"
    ]
    func startTimer(){
        timerFunction?.cancel()
        timerFunction = DispatchWorkItem {
            self.getPokemon()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 30.0, execute: timerFunction ?? DispatchWorkItem{print("error")})
    }
    func getPokemon(){
        startTimer()
        let randomNumber = String(Int.random(in: 1...1010))
        let urlString = "https://pokeapi.co/api/v2/pokemon/" + randomNumber
        NetworkServices.shared.getData(url: urlString, completion: {res, error in
            if let result = res{
                let decodedResult: Result<PokedexModel, Error> = NetworkServices.shared.decodeJSON(data: result, type: PokedexModel.self)
                switch decodedResult {
                case .success(let decodedData):
                    self.processPokemon(data: decodedData)
                case .failure(let error):
                    print("Error decoding JSON: \(error)")
                }
            }
        })
    }
    func processPokemon(data: PokedexModel){
        GetImageService.shared.getImages(imageURL: data.sprites.other.officialArtwork.frontDefault) { res in
            if let image = res{
                self.pokemonImage = image
                self.pokemonName = data.name
                self.pokemonType = data.types[0].type.name
                if let translation = self.getValueForKey(data.types[0].type.name, in: self.translateOptions) {
                    self.pokemonTypeTranslated = translation
                } else {
                    self.pokemonTypeTranslated = data.types[0].type.name
                }
                self.pokemonHeight = String(Float(data.height)/10)
                self.pokemonWeight = String(Float(data.weight)/10)
                self.delegate?.updateUI()
            }
            
        }
    }
    func getValueForKey(_ key: String, in translateOptions: [String: String]) -> String? {
        return translateOptions[key]
    }
}
