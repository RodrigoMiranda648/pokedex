//
//  ViewController.swift
//  Pokedex
//
//  Created by Rodrigo Miranda Castillo on 31/08/23.
//

import UIKit

class PokedexViewController: UIViewController {

    private let viewModel = PokedexViewModelImp()
    private let circleRadius: CGFloat = 120
    
    private let upperViewContainer:     UIView = {
        let view = UIView()
        view.backgroundColor = .systemGray
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        view.layer.cornerRadius = 20
        view.layer.shadowColor = UIColor(named: "pokedexDark")!.cgColor
        view.layer.shadowOpacity = 0.8
        view.layer.shadowOffset = CGSize(width: 5, height: 5)
        view.layer.shadowRadius = 8
        view.layer.masksToBounds = false
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let darkUpperView:          UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 1, alpha: 0.35)
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let mediumLineView:         UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(named: "pokedexDark")
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let circleView:             UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.borderWidth = 20
        view.layer.borderColor = UIColor(named: "pokedexDark")!.cgColor
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let lowerViewContainer:     UIView = {
        let view = UIView()
        view.backgroundColor = .systemGray5
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.layer.cornerRadius = 20
        view.layer.borderColor = UIColor.systemGray.cgColor
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.8
        view.layer.borderWidth = 2
        view.layer.shadowOffset = CGSize(width: 5, height: 5)
        view.layer.shadowRadius = 5
        view.layer.masksToBounds = false
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let stackContainerView:     UIView = {
        let view = UIView()
        view.backgroundColor = .darkGray
        view.layer.cornerRadius = 20
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let pokedexLabel:           UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor(named: "pokedexDark")
        lbl.text = "Pokedex"
        lbl.textColor = UIColor.white
        lbl.font = UIFont.systemFont(ofSize: 30)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.numberOfLines = 1
        lbl.textAlignment = .center
        lbl.layer.cornerRadius = 20
        lbl.clipsToBounds = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let pokemonNameLabel:       UILabel = {
        let lbl = UILabel()
        lbl.text = "Nombre".uppercased()
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 30)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.numberOfLines = 1
        lbl.isHidden = false
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let pokemonHeightLabel:     UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 20)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.numberOfLines = 1
        lbl.isHidden = false
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let pokemonWeightLabel:     UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 20)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.numberOfLines = 1
        lbl.isHidden = false
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let pokemonTypeLabel:       UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.textColor = UIColor.white
        lbl.font = UIFont.systemFont(ofSize: 20)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.numberOfLines = 1
        lbl.textAlignment = .center
        lbl.layer.cornerRadius = 20
        lbl.clipsToBounds = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let heightLabel:            UILabel = {
        let lbl = UILabel()
        lbl.text = "ALTURA"
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 20)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.numberOfLines = 1
        lbl.isHidden = false
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let weightLabel:            UILabel = {
        let lbl = UILabel()
        lbl.text = "PESO"
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 20)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.numberOfLines = 1
        lbl.isHidden = false
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let typeLabel:              UILabel = {
        let lbl = UILabel()
        lbl.text = "TIPO"
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 20)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.numberOfLines = 1
        lbl.isHidden = false
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private var pokemonImageView =      UIImageView()
    private let pokemonHeightView:      UIImageView = {
        let view = UIImageView(image: UIImage(named: "figure.wave"))
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let pokemonWeightView:      UIImageView = {
        let view = UIImageView(image: UIImage(named: "scalemass"))
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let verticalStack:          UIStackView = {
        let stack = UIStackView()
        stack.backgroundColor = .clear
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing = 10
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    private let firstHorizontalStack:   UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 5
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    private let secondHorizontalStack:  UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 5
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    private lazy var getPokemonButton:  UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named: "pokedex")
        button.setTitle("Obtener Pokémon", for: .normal)
        button.addTarget(self, action: #selector(getPokemonButtonTapped), for: .touchUpInside)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        button.layer.cornerRadius = 10
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOpacity = 0.5
        button.layer.shadowOffset = CGSize(width: 5, height: 5)
        button.layer.shadowRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    private var pokemonImage:           UIImage?{
        didSet{
            pokemonImageView.image = pokemonImage
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = UIColor(named: "pokedex")
        viewModel.delegate = self
        setUpCircle()
        setUpViewComponents()
        setUpImage()
        viewModel.getPokemon()
    }
    
    private func setUpCircle(){
        circleView.layer.cornerRadius = circleRadius
    }
    private func setUpViewComponents(){
        view.addSubview(circleView)
        view.addSubview(upperViewContainer)
        view.addSubview(darkUpperView)
        view.addSubview(lowerViewContainer)
        view.addSubview(mediumLineView)
        view.addSubview(pokedexLabel)
        view.addSubview(getPokemonButton)
        view.addSubview(pokemonImageView)
        view.addSubview(verticalStack)
        
        view.addSubview(stackContainerView)
        stackContainerView.addSubview(verticalStack)
        verticalStack.addArrangedSubview(pokemonNameLabel)
        verticalStack.addArrangedSubview(firstHorizontalStack)
        verticalStack.addArrangedSubview(secondHorizontalStack)
        
        firstHorizontalStack.addArrangedSubview(pokemonHeightLabel)
        firstHorizontalStack.addArrangedSubview(pokemonWeightLabel)
        firstHorizontalStack.addArrangedSubview(pokemonTypeLabel)
        secondHorizontalStack.addArrangedSubview(heightLabel)
        secondHorizontalStack.addArrangedSubview(weightLabel)
        secondHorizontalStack.addArrangedSubview(typeLabel)
        
        NSLayoutConstraint.activate([
            circleView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            circleView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
            circleView.heightAnchor.constraint(equalToConstant: circleRadius*2),
            circleView.widthAnchor.constraint(equalToConstant: circleRadius*2),
            
            upperViewContainer.topAnchor.constraint(equalTo: view.topAnchor),
            upperViewContainer.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor, constant: -50),
            upperViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            upperViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            darkUpperView.topAnchor.constraint(equalTo: view.topAnchor),
            darkUpperView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            darkUpperView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            darkUpperView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            
            mediumLineView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
            mediumLineView.heightAnchor.constraint(equalToConstant: 20),
            mediumLineView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mediumLineView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            pokedexLabel.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
            pokedexLabel.heightAnchor.constraint(equalToConstant: 50),
            pokedexLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            pokedexLabel.widthAnchor.constraint(equalToConstant: 150),
            
            lowerViewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            lowerViewContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor, constant: 50),
            lowerViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            lowerViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            
            pokemonImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            pokemonImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
            pokemonImageView.widthAnchor.constraint(equalToConstant: 300),
            pokemonImageView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor, constant: -100),
            
            stackContainerView.topAnchor.constraint(equalTo: lowerViewContainer.topAnchor, constant: 20),
            stackContainerView.leadingAnchor.constraint(equalTo: lowerViewContainer.leadingAnchor, constant: 20),
            stackContainerView.trailingAnchor.constraint(equalTo: lowerViewContainer.trailingAnchor, constant: -20),
            stackContainerView.heightAnchor.constraint(equalToConstant: 150),
            
            verticalStack.topAnchor.constraint(equalTo: stackContainerView.topAnchor, constant: 5),
            verticalStack.leadingAnchor.constraint(equalTo: stackContainerView.leadingAnchor, constant: 5),
            verticalStack.trailingAnchor.constraint(equalTo: stackContainerView.trailingAnchor, constant: -5),
            verticalStack.bottomAnchor.constraint(equalTo: stackContainerView.bottomAnchor, constant: -5),
            
            getPokemonButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            getPokemonButton.topAnchor.constraint(equalTo: verticalStack.bottomAnchor, constant: 50),
            getPokemonButton.widthAnchor.constraint(equalToConstant: 200),
            getPokemonButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    private func setUpImage(){
        pokemonImageView.translatesAutoresizingMaskIntoConstraints = false
        pokemonImageView.contentMode = .scaleAspectFit
    }
    
    @objc private func getPokemonButtonTapped(){
        viewModel.getPokemon()
    }
}
extension PokedexViewController: PokedexViewModel{
    func updateUI() {
        guard let pokemonName           = viewModel.pokemonName,
              let pokemonImage          = viewModel.pokemonImage,
              let pokemonType           = viewModel.pokemonType,
              let pokemonHeight         = viewModel.pokemonHeight,
              let pokemonWeight         = viewModel.pokemonWeight,
              let pokemonTypeTransalted = viewModel.pokemonTypeTranslated
        else{return}
        DispatchQueue.main.async {
            self.pokemonNameLabel.text              = pokemonName.uppercased()
            self.pokemonNameLabel.isHidden          = false
            self.pokemonImage                       = pokemonImage
            self.pokemonHeightLabel.text            = pokemonHeight + " m"
            self.pokemonWeightLabel.text            = pokemonWeight + " kg"
            self.upperViewContainer.backgroundColor = UIColor(named: pokemonType) ?? UIColor(named: "default")
            self.stackContainerView.backgroundColor   = UIColor(named: pokemonType) ?? UIColor(named: "default")
            self.pokemonTypeLabel.text              = pokemonTypeTransalted
            switch pokemonType {
            case "electric","ice","normal","flying":
                self.pokemonTypeLabel.textColor     = .black
                self.pokemonNameLabel.textColor     = .black
                self.pokemonHeightLabel.textColor   = .black
                self.pokemonWeightLabel.textColor   = .black
                self.heightLabel.textColor          = .black
                self.weightLabel.textColor          = .black
                self.typeLabel.textColor            = .black
            default:
                self.pokemonTypeLabel.textColor     = .white
                self.pokemonNameLabel.textColor     = .white
                self.pokemonHeightLabel.textColor   = .white
                self.pokemonWeightLabel.textColor   = .white
                self.heightLabel.textColor          = .white
                self.weightLabel.textColor          = .white
                self.typeLabel.textColor            = .white
            }
            switch pokemonType{
            case "dark","ghost", "fighting":
                self.darkUpperView.isHidden = false
            default:
                self.darkUpperView.isHidden = true
            }
        }
    }
}

