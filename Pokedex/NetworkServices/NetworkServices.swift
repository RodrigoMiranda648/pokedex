//
//  NetworkServices.swift
//  Pokedex
//
//  Created by Rodrigo Miranda Castillo on 31/08/23.
//

import Foundation
import UIKit

class NetworkServices {
    //Singleton
    static var shared: NetworkServices = NetworkServices()
    
    func getData(url: String, completion: @escaping(Data?, Error?) -> ()){
        let urlString: String = url
        guard let url = URL(string: urlString) else {return}
        URLSession.shared.dataTask(with: url){data, response, error in
            if let error = error{
                print(error.localizedDescription)
                completion(nil, error)
            }
            if let data = data{
                completion(data, nil)
            }
        }.resume()
    }
    func decodeJSON<T: Decodable>(data: Data, type: T.Type) -> Result<T, Error>{
        do{
            let objects = try JSONDecoder().decode(type.self, from: data)
            return .success(objects)
        }catch{
            print(error.localizedDescription)
            return .failure(error)
        }
    }
}

class GetImageService {
    static var shared: GetImageService = GetImageService()
    
    func getImages(imageURL: String, completion: @escaping(UIImage?) -> ()){
        let urlString = String(imageURL)
        guard let url = URL(string: urlString)else{return}
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error{
                print("error:")
                print(error.localizedDescription)
                completion(nil)
            }
            if let data = data{
                let image = UIImage(data: data)
                completion(image ?? UIImage())
            }
        }.resume()
    }
}
